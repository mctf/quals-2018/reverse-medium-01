object KeygenForm: TKeygenForm
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Keygen'
  ClientHeight = 114
  ClientWidth = 295
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 69
    Top = 19
    Width = 29
    Height = 13
    Caption = 'Code:'
  end
  object Label2: TLabel
    Left = 48
    Top = 51
    Width = 50
    Height = 13
    Caption = 'Password:'
  end
  object CodeEdit: TEdit
    Left = 104
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object PasswordEdit: TEdit
    Left = 104
    Top = 43
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object GenerateButton: TButton
    Left = 120
    Top = 77
    Width = 75
    Height = 25
    Caption = 'Generate'
    TabOrder = 2
    OnClick = GenerateButtonClick
  end
end
