//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TKeygenForm : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TEdit *CodeEdit;
	TLabel *Label2;
	TEdit *PasswordEdit;
	TButton *GenerateButton;
	void __fastcall GenerateButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TKeygenForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TKeygenForm *KeygenForm;
//---------------------------------------------------------------------------
#endif
