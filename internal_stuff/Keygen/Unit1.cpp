//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TKeygenForm *KeygenForm;
//---------------------------------------------------------------------------
__fastcall TKeygenForm::TKeygenForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TKeygenForm::GenerateButtonClick(TObject *Sender)
{
	String password;
	password.SetLength(10);
	srand(StrToInt(CodeEdit->Text));

	for (int i = 1; i <= 10; ++i)
	{
		password[i] = rand() % 43 + 48;
	}

	PasswordEdit->Text = password;
}
//---------------------------------------------------------------------------
