//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
//---------------------------------------------------------------------------
class TBannerForm : public TForm
{
__published:	// IDE-managed Components
	TTimer *Timer1;
	TMemo *BannerTextMemo;
	TLabel *BlockLabel;
	TLabel *SMSLabel;
	TLabel *UnlockCodeLabel;
	TLabel *NumberLabel;
	TEdit *PasswordEdit;
	TButton *OKButton;

	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall OKButtonClick(TObject *Sender);
private:
	String password;	// User declarations
public:		// User declarations
	__fastcall TBannerForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBannerForm *BannerForm;
//---------------------------------------------------------------------------
#endif
