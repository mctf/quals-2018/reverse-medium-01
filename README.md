# Description
It is necessary to find the password for the code 1536568435. The flag will be a string like mctf{md5sum(password)}. RUN STRICTLY ON A VIRTUAL MACHINE! The task executable file can be detected by antivirus software as malicious. Password for archive: infected.

# Writeup
Баннер-вымогатель. Написан на C++, скомпилен с оптимизацией при помощи Embarcadero C++ Builder (https://www.embarcadero.com/ru/products/cbuilder/starter). Требует ввода пароля для закрытия окна. Блокирует работу (держит фокус на себе). Есть простейшая антиотладка (IsDebuggerPresent).

Подход хорошо описан в статье https://xakep.ru/2008/01/17/41931/. В области данных (.data) находится ссылка на метод создания формы (FormCreate), в нем весь код. В нем генератор псевдослучайных чисел иницицлизируется текущим временем, которое и выводится на форму в качестве кода. Дальше из последовательности псевдослучайных чисел получается 10-ти символьный пароль элементарным "добиванием" до печатного символа. Нужно просто закодить тоже самое в C++ Builder (можно скачать бесплатно с сайта https://www.embarcadero.com/ru/products/cbuilder/starter) и получить кейген.